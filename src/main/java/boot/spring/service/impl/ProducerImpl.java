package boot.spring.service.impl;

import boot.spring.po.Mail;
import boot.spring.service.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Service;

import javax.jms.Destination;

@Service("producer")
public class ProducerImpl implements Producer {

    @Autowired
    public JmsMessagingTemplate jmsMessagingTemplate;


    @Override
    public void sendMail(Destination des, Mail mail) {
        jmsMessagingTemplate.convertAndSend(des, mail);
    }


}
