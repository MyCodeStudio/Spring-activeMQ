package boot.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.jms.annotation.EnableJms;


/**
 * 启动类
 *
 * @author zhangqiangqiang14
 */
@SpringBootApplication(scanBasePackages = {"boot.spring"})
@EnableJms
@ServletComponentScan
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}
