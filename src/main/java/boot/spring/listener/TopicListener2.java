package boot.spring.listener;

import boot.spring.po.Mail;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/**
 * @author zhangqiangqiang14
 */
@Component
public class TopicListener2 {

    @JmsListener(destination = "mytopic", containerFactory = "jmsListenerContainerTopic")
    public void displayTopic(Mail msg) {
        System.out.println("consumer2从ActiveMQ的Topic：mytopic中取出一条消息：");
        System.out.println(msg);
    }
}
