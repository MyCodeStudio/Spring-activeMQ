package boot.spring.listener;

import boot.spring.po.Mail;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class QueueListener2 {

    @JmsListener(destination = "myqueue", containerFactory = "jmsListenerContainerQueue")
    public void displayMail(Mail mail) {
        System.out.println("listen2从ActiveMQ队列myqueue中取出一条消息：");
        System.out.println(mail.toString());
    }
}
